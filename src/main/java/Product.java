
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author napas
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;
    
    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    public static ArrayList<Product> genProductList(){
        ArrayList<Product>list = new ArrayList<>();
        list.add(new Product(1,"Espresso1",35,"1.jpg"));
        list.add(new Product(1,"Espresso2",40,"1.jpg"));
        list.add(new Product(1,"Capucino1",50,"2.jpg"));
        list.add(new Product(1,"Capucino2",60,"2.jpg"));
        list.add(new Product(1,"Americano1",40,"3.jpg"));
        list.add(new Product(1,"Americano2",50,"3.jpg"));
        list.add(new Product(1,"Thai Tea1",60,"4.jpg"));
        list.add(new Product(1,"Thai Tea2",70,"4.jpg"));
        return list;
    }
}
